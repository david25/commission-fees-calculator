import axios from 'axios';
import { LEGAL } from './helpers/constants';

export const HOST_NAME = 'http://private-38e18c-uzduotis.apiary-mock.com';
export const DEFAULT_PATH = '/config/cash-in';

export const getDataFromAPI = async (path) => {
  const currentPath = path || DEFAULT_PATH;
  const response = await axios.get(`${HOST_NAME}${currentPath}`);

  return response.data;
};

export const fetchCashInCommission = async () => {
  return getDataFromAPI();
};

export const fetchCashOutCommission = async (mode) => {
  if (mode === LEGAL) {
    return getDataFromAPI('/config/cash-out/juridical');
  }

  return getDataFromAPI('/config/cash-out/natural');
};
