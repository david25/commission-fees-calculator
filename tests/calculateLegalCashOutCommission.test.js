import calculateLegalCashOutCommission from '../calculations/calculateLegalCashOutCommission';

const cashOutLegalCommissionData = {
  percents: 0.3,
  min: {
    amount: 0.5,
    currency: 'EUR',
  },
};

describe('calculateLegalCashOutCommission', () => {
  test('it should pcalculate the cash out commission for legal / juridical persons', () => {
    const data = {
      date: '2016-01-06',
      user_id: 2,
      user_type: 'juridical',
      type: 'cash_out',
      operation: { amount: 300.0, currency: 'EUR' },
    };

    const operationAmount = data.operation.amount;

    const commission = calculateLegalCashOutCommission(cashOutLegalCommissionData, operationAmount);

    expect(commission).toBe(0.9);
  });
});
