import formatNumber from '../helpers/formatNumber';

describe('formatNumber', () => {
  test('it should round up and convert the number to string', () => {
    expect(formatNumber(23.542222)).toBe('23.54');
  });

  test('it adds .00 to a whole number', () => {
    expect(formatNumber(93)).toBe('93.00');
  });
});
