import calculateNaturalCashOutCommission from '../calculations/calculateNaturalCashOutCommission';

const cashOutNaturalCommissionData = {
  percents: 0.3,
  week_limit: {
    amount: 1000,
    currency: 'EUR',
  },
};

describe('calculateNaturalCashOutCommission', () => {
  test('it should calculate the cash out commission for natural persons', () => {
    const data = {
      date: '2016-01-06',
      user_id: 1,
      user_type: 'natural',
      type: 'cash_out',
      operation: { amount: 30000, currency: 'EUR' },
    };

    const commission = calculateNaturalCashOutCommission(cashOutNaturalCommissionData, data);

    expect(commission).toBe(87);
  });

  test('it should properly give weekly discounts to natural persons', () => {
    const userData = [
      {
        date: '2016-02-15',
        user_id: 1,
        user_type: 'natural',
        type: 'cash_out',
        operation: { amount: 300.0, currency: 'EUR' },
      },
      {
        date: '2016-02-16',
        user_id: 1,
        user_type: 'natural',
        type: 'cash_out',
        operation: { amount: 800.0, currency: 'EUR' },
      },
      {
        date: '2016-02-17',
        user_id: 1,
        user_type: 'natural',
        type: 'cash_out',
        operation: { amount: 700.0, currency: 'EUR' },
      },
    ];

    const results = [];
    userData.map((data) => {
      return results.push(calculateNaturalCashOutCommission(cashOutNaturalCommissionData, data));
    });

    expect(results).toEqual([0, 0.3, 2.1]);
  });
});
