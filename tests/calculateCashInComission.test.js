import calculateCashInCommission from '../calculations/calculateCashInCommission';

const defaultCommissionFee = 0.03;
const maxCashInCommission = 5;

describe('Calculate cash in commission', () => {
  test('Should calculate the cash in commission', () => {
    const amount = 200;
    const commission = calculateCashInCommission(defaultCommissionFee, amount, maxCashInCommission);

    expect(commission).toBe(0.06);
  });

  test('The commission should not be more than 5.00 EUR', () => {
    const commission = calculateCashInCommission(defaultCommissionFee, 50000, maxCashInCommission);

    expect(commission).toBe(5.0);
  });
});
