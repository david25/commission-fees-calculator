import mockAxios from 'axios';
import {
  getDataFromAPI,
  fetchCashInCommission,
  fetchCashOutCommission,
  HOST_NAME,
  DEFAULT_PATH,
} from '../dataFetcher';
import { LEGAL } from '../helpers/constants';

const mockCashInData = {
  percents: 0.03,
  max: {
    amount: 5,
    currency: 'EUR',
  },
};

const mockNaturalCashOutCommission = {
  percents: 0.3,
  week_limit: {
    amount: 1000,
    currency: 'EUR',
  },
};

const mockLegalCashOutCommission = {
  percents: 0.3,
  min: {
    amount: 0.5,
    currency: 'EUR',
  },
};

describe('Fetch data from the API', () => {
  describe('getDataFromAPI', () => {
    test("it should call the /cash-in endpoint when there's no parameters", async () => {
      mockAxios.get.mockImplementationOnce(() =>
        Promise.resolve({
          data: mockCashInData,
        })
      );

      await getDataFromAPI();

      const path = `${HOST_NAME}${DEFAULT_PATH}`;

      expect(mockAxios.get).toHaveBeenCalledWith(path);
    });
  });

  describe('fetchCashInCommission', () => {
    test('it should call the cash-in commission endpoint and get the cash-in data', async () => {
      mockAxios.get.mockImplementationOnce(() =>
        Promise.resolve({
          data: mockCashInData,
        })
      );

      const response = await fetchCashInCommission();

      expect(response.percents).toBe(0.03);
    });
  });

  describe('fetchCashOutCommission', () => {
    test("it should call the cash-out commission endpoint and get the cash-out data for natural persons when there's no parameters passed", async () => {
      mockAxios.get.mockImplementationOnce(() =>
        Promise.resolve({
          data: mockNaturalCashOutCommission,
        })
      );

      const response = await fetchCashOutCommission();

      expect(response).toMatchObject(mockNaturalCashOutCommission);
    });

    test('it should call the cash-out commission endpoint and get the cash-out data for legal persons when /config/cash-out/juridical is passed', async () => {
      mockAxios.get.mockImplementationOnce(() =>
        Promise.resolve({
          data: mockLegalCashOutCommission,
        })
      );

      const response = await fetchCashOutCommission(LEGAL);

      expect(response).toMatchObject(mockLegalCashOutCommission);
    });
  });
});
