export default function formatNumber(number) {
  const roundedNumber = Math.round(number * 100.0) / 100.0;

  const formattedNumber = roundedNumber
    .toString()
    .replace(/^(\d+)$/, '$1.00')
    .replace(/^-(\d+)$/, '-$1.00')
    .replace(/^(\d+)\.(\d)$/, '$1.$20')
    .replace(/^-(\d+)\.(\d)$/, '-$1.$20');

  return formattedNumber;
}
