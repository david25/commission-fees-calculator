import moment from 'moment';

/** All the deduction logs are in here */
const hasAlreadyDeductedWeeks = [];

/** When a natural person does a transaction, it will be logged using this function */
const logDeduction = (userId, transactionWeek, operationAmount, maxDiscount) => {
  const found = hasAlreadyDeductedWeeks.find(
    (obj) => obj.userId === userId && obj.transactionWeek === transactionWeek
  );

  if (found) {
    if (found.usedDiscount >= maxDiscount) {
      return;
    }

    if (operationAmount > maxDiscount) {
      found.usedDiscount = maxDiscount;
      return;
    }

    if (operationAmount <= maxDiscount) {
      found.usedDiscount = operationAmount;
      return;
    }
  }

  if (operationAmount >= maxDiscount) {
    hasAlreadyDeductedWeeks.push({ userId, transactionWeek, usedDiscount: maxDiscount });
  } else {
    hasAlreadyDeductedWeeks.push({ userId, transactionWeek, usedDiscount: operationAmount });
  }
};

const getCommission = (percentage, amount) => {
  return (percentage * amount) / 100;
};

const calculateNaturalCashOutCommission = (cashOutNaturalCommissionData, data) => {
  /** Cash out commission calculation for natural persons */

  const userId = data.user_id;
  const { date } = data;
  const naturalCommissionPercentage = cashOutNaturalCommissionData.percents;
  const maxDiscount = cashOutNaturalCommissionData.week_limit.amount;
  const operationAmount = data.operation.amount;

  const transactionWeek = moment(new Date(date)).isoWeek();

  const hasPreviousTransactionWithFreeChargeAmount = hasAlreadyDeductedWeeks.find(
    (obj) =>
      obj.userId === userId &&
      obj.transactionWeek === transactionWeek &&
      obj.usedDiscount <= maxDiscount
  );

  if (!hasPreviousTransactionWithFreeChargeAmount) {
    /* If the operationAmount is the same as the maxDiscount && the user has no previous transaction yet */
    if (operationAmount === maxDiscount) {
      logDeduction(userId, transactionWeek, operationAmount, maxDiscount);

      return operationAmount - maxDiscount;
    }

    if (operationAmount < maxDiscount) {
      logDeduction(userId, transactionWeek, operationAmount, maxDiscount);
      return 0;
    }

    if (operationAmount > maxDiscount) {
      const remainingAmount = operationAmount - maxDiscount;

      logDeduction(userId, transactionWeek, operationAmount, maxDiscount);
      return getCommission(naturalCommissionPercentage, remainingAmount);
    }
  }

  if (hasPreviousTransactionWithFreeChargeAmount) {
    const { usedDiscount } = hasPreviousTransactionWithFreeChargeAmount;

    /* If the user has already reached the maxDiscount amount for the week, return without discount */
    if (usedDiscount >= maxDiscount) {
      return getCommission(naturalCommissionPercentage, operationAmount);
    }

    /* If the user haven't reached the maxDiscount amount for the week yet, get the difference between the operationAmount and the available discount to get the commission */
    if (usedDiscount <= maxDiscount) {
      const discount = Math.abs(usedDiscount - maxDiscount);
      logDeduction(userId, transactionWeek, operationAmount + usedDiscount, maxDiscount);
      return getCommission(naturalCommissionPercentage, Math.abs(operationAmount - discount));
    }
  }

  return null;
};

export default calculateNaturalCashOutCommission;
