/** Cash out commission calculation for legal persons */
const calculateLegalCashOutCommission = (cashOutLegalCommissionData, operationAmount) => {
  const legalCommissionPercentage = cashOutLegalCommissionData.percents;
  const minLegalCashOutCommissionFee = cashOutLegalCommissionData.min.amount;
  const totalLegalCashOutCommission = (legalCommissionPercentage * operationAmount) / 100;

  if (totalLegalCashOutCommission < minLegalCashOutCommissionFee) {
    return minLegalCashOutCommissionFee;
  }

  return totalLegalCashOutCommission;
};

export default calculateLegalCashOutCommission;
