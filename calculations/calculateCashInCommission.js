const calculateCashInCommission = (percents, amount, maxCashInCommissionFee) => {
  const totalCashInCommission = (percents * amount) / 100;

  if (totalCashInCommission > maxCashInCommissionFee) {
    return maxCashInCommissionFee;
  }

  return totalCashInCommission;
};

export default calculateCashInCommission;
