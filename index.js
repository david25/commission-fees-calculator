import fs from 'fs';
import process from 'process';
import { fetchCashInCommission, fetchCashOutCommission } from './dataFetcher';
import calculateCashInCommission from './calculations/calculateCashInCommission';
import { LEGAL, CURRENCY_SUPPORT_ERROR } from './helpers/constants';
import formatNumber from './helpers/formatNumber';
import calculateLegalCashOutCommission from './calculations/calculateLegalCashOutCommission';
import calculateNaturalCashOutCommission from './calculations/calculateNaturalCashOutCommission';

const allowedCurrencies = ['EUR'];

const calculateCommissionFees = async () => {
  const inputFileDir = process.argv[2];
  const userData = JSON.parse(fs.readFileSync(inputFileDir, 'utf8'));

  const CASH_OUT = 'cash_out';
  const CASH_IN = 'cash_in';
  const cashOutNaturalCommissionData = await fetchCashOutCommission();
  const cashOutLegalCommissionData = await fetchCashOutCommission(LEGAL);
  const cashInCommissionData = await fetchCashInCommission();

  userData.map((data) => {
    const { amount, currency } = data.operation;

    if (allowedCurrencies.indexOf(currency) === -1) {
      return process.stdout.write(`${CURRENCY_SUPPORT_ERROR}\n`);
    }

    if (data.type === CASH_IN) {
      const { percents } = cashInCommissionData;
      const maxCashInCommissionFee = cashInCommissionData.max.amount;

      const totalCashInCommission = formatNumber(
        calculateCashInCommission(percents, amount, maxCashInCommissionFee)
      );

      return process.stdout.write(`${totalCashInCommission}\n`);
    }

    if (data.type === CASH_OUT) {
      const userType = data.user_type;

      const operationAmount = data.operation.amount;

      let totalCashOutCommission = calculateNaturalCashOutCommission(
        cashOutNaturalCommissionData,
        data
      );

      if (userType === LEGAL) {
        totalCashOutCommission = calculateLegalCashOutCommission(
          cashOutLegalCommissionData,
          operationAmount
        );
      }

      const formattedTotalCommission = formatNumber(totalCashOutCommission);

      return process.stdout.write(`${formattedTotalCommission}\n`);
    }

    return 'Invalid transaction type';
  });
};

calculateCommissionFees();
